
# ChangeLog

## Unreleased

**Added**
* Licence

**Changed:**
* Updated README to align with template used in other services

**Removed**
* Hot-deployment scripts

## [2.2.0](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/2.2.0) (2020-07-01)

**Added:**
* REST API for refreshing single catalogue
* REST API for deleting single catalogue metrics
 
## [2.1.0](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/2.1.0) (2020-06-15)

**Added:**
* Warnings in SHACL results
* Severity in SHACL violation reports

## [2.0.1](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/2.0.1) (2020-05-29)

**Fixed:**
* Missing time values for status code errors

## [2.0.0](https://gitlab.fokus.fraunhofer.de/viaduct/piveau-transforming-js/tags/2.0.0) (2020-05-25)

**Changed:**
* Almost everything, except the API itself

**Added:**

**Changed:**
* dateIssuedAvailability is calculated only on Datasets
* Apikey now must be set via environment variables

**Removed:**

**Fixed:**
* Queries for violations
