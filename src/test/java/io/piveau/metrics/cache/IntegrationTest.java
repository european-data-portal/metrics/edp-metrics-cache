package io.piveau.metrics.cache;

import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import io.piveau.metrics.cache.persistence.DocumentScope;
import io.vertx.core.*;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.client.predicate.ResponsePredicate;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static io.piveau.metrics.cache.ApplicationConfig.*;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(VertxExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class IntegrationTest {

    private WebClient webClient;
    private JsonObject testMetrics, testInfo;

    @BeforeAll
    void setup(Vertx vertx, VertxTestContext testContext) {
        try {
            WebClientOptions clientOptions = new WebClientOptions()
                    .setDefaultHost("127.0.0.1")
                    .setDefaultPort(8080);
            webClient = WebClient.create(vertx, clientOptions);

            String host = DEFAULT_MONGODB_SERVER_HOST;
            int port = 27027; // use non-default port to prevent conflicts when developing

            MongodStarter starter = MongodStarter.getDefaultInstance();
            IMongodConfig mongodConfig = new MongodConfigBuilder()
                    .version(Version.Main.PRODUCTION)
                    .net(new Net(host, port, Network.localhostIsIPv6()))
                    .build();

            starter.prepare(mongodConfig).start();

            JsonObject config = new JsonObject()
                    .put("host", host)
                    .put("port", port);

            MongoClient dbClient = MongoClient.createShared(vertx, config);

            testMetrics = generateMetrics();
            testInfo = generateInfo();
            initDBTestData(dbClient, testContext.succeeding(initDb -> {
                JsonObject testConfig = new JsonObject()
                        .put(ENV_APPLICATION_PORT, 8080)
                        .put(ENV_MONGODB_SERVER_HOST, DEFAULT_MONGODB_SERVER_HOST)
                        .put(ENV_MONGODB_SERVER_PORT, DEFAULT_MONGODB_SERVER_PORT)
                        .put(ENV_APIKEY, "test-dummy");

                vertx.deployVerticle(MainVerticle.class.getName(), new DeploymentOptions().setConfig(testConfig), testContext.completing());
            }));

        } catch (Exception e) {
            testContext.failNow(e);
        }
    }


    @Test
    void testRetrieveGlobalMetrics(VertxTestContext testContext) {
        webClient.get("/metrics/global")
                .expect(ResponsePredicate.SC_OK)
                .expect(ResponsePredicate.JSON)
                .send(testContext.succeeding(response -> testContext.verify(() -> {
                    assertEquals(testMetrics, response.bodyAsJsonObject());
                    testContext.completeNow();
                })));
    }

    /*
        @Test
        void testRetrieveScore(VertxTestContext testContext) {
            webClient.get("/score/5de12b5ecebc9b334023e033?startDate=2015-11-09&endDate=2019-11-09&resolution=month")
                .expect(ResponsePredicate.SC_OK)
                .send(testContext.succeeding(response -> testContext.verify(() -> {
                    assertEquals(new JsonObject("{\"Result\": [{\"2017-01\": 1768},{\"2017-02\": 318},{\"2017-04\": 318},{\"2019-11\": 93}]}"), response.bodyAsJsonObject());
                    testContext.completeNow();
                })));
        }
    */
    @Test
    void testRetrieveCatalogues(VertxTestContext testContext) {
        webClient.get("/metrics/catalogues/")
                .expect(ResponsePredicate.SC_OK)
                .expect(ResponsePredicate.JSON)
                .send(testContext.succeeding(response -> testContext.verify(() -> {
                    assertEquals(1, response.bodyAsJsonArray().size());
                    assertEquals("testCatalogue", response.bodyAsJsonArray().getString(0));
                    testContext.completeNow();
                })));
    }

    @Test
    void testRetrieveCatalogueMetrics(VertxTestContext testContext) {
        JsonObject testCat = testMetrics.copy().put("info", getInfoObject());
        webClient.get("/metrics/catalogues/testCatalogue")
                .expect(ResponsePredicate.SC_OK)
                .expect(ResponsePredicate.JSON)
                .send(testContext.succeeding(response -> testContext.verify(() -> {
                    assertEquals(testCat, response.bodyAsJsonObject());
                    testContext.completeNow();
                })));
    }

    @Test
    void testRetrieveCountries(VertxTestContext testContext) {
        webClient.get("/metrics/countries/")
                .expect(ResponsePredicate.SC_OK)
                .expect(ResponsePredicate.JSON)
                .send(testContext.succeeding(response -> testContext.verify(() -> {
                    assertEquals(1, response.bodyAsJsonArray().size());
                    assertEquals("testCountry", response.bodyAsJsonArray().getString(0));
                    testContext.completeNow();
                })));
    }

    @Test
    void testRetrieveCountryMetrics(VertxTestContext testContext) {
        webClient.get("/metrics/countries/testCountry")
                .expect(ResponsePredicate.SC_OK)
                .expect(ResponsePredicate.JSON)
                .send(testContext.succeeding(response -> testContext.verify(() -> {
                    assertEquals(testMetrics, response.bodyAsJsonObject());
                    testContext.completeNow();
                })));
    }

    @Test
    void testRetrieveCatalogueInfo(VertxTestContext testContext) {
        webClient.get("/info/catalogues")
                .expect(ResponsePredicate.SC_OK)
                .expect(ResponsePredicate.JSON)
                .send(testContext.succeeding(response ->
                        testContext.verify(() -> {
                            assertEquals(1, response.bodyAsJsonArray().size());
                            assertEquals(testInfo, response.bodyAsJsonArray().getJsonObject(0));
                            testContext.completeNow();
                        })));
    }

    private void initDBTestData(MongoClient dbClient, Handler<AsyncResult<CompositeFuture>> resultHandler) {
        List<Future> dbFutures = new ArrayList<>();

        Promise<String> globalMetrics = Promise.promise();
        dbClient.save(DocumentScope.GLOBAL.name(), testMetrics.copy().put("_id", "global"), globalMetrics);
        dbFutures.add(globalMetrics.future());

        Promise<String> catalogueMetrics = Promise.promise();
        dbClient.save(DocumentScope.CATALOGUE.name(), testMetrics.copy().put("_id", "testCatalogue").put("info", getInfoObject()), catalogueMetrics);
        dbFutures.add(catalogueMetrics.future());
/*
        String jsonStr = null;
        try {
            jsonStr = IOUtils.toString(new FileReader("misc/dummyD/mockScore.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert jsonStr != null;
        JsonObject result = new JsonObject(jsonStr);
        Promise<String> scoreMetrics = Promise.promise();
        dbClient.save(MetricType.SCORE.name(), result, scoreMetrics);
        dbFutures.add(scoreMetrics.future());
*/
        Promise<String> countryMetrics = Promise.promise();
        dbClient.save(DocumentScope.COUNTRY.name(), testMetrics.copy().put("_id", "testCountry"), countryMetrics);
        dbFutures.add(countryMetrics.future());

        CompositeFuture.all(dbFutures).setHandler(resultHandler);
    }

    private JsonObject generateMetrics() {


        JsonObject findability = new JsonObject()
                .put("keywordAvailability", getYesNoPercentage())
                .put("categoryAvailability", getYesNoPercentage())
                .put("spatialAvailability", getYesNoPercentage())
                .put("temporalAvailability", getYesNoPercentage());

        JsonArray statusCodes = new JsonArray()
                .add(new JsonObject().put("name", "200").put("percentage", 70d))
                .add(new JsonObject().put("name", "404").put("percentage", 20d))
                .add(new JsonObject().put("name", "500").put("percentage", 10d));

        JsonObject accessibility = new JsonObject()
                .put("accessUrlStatusCodes", statusCodes)
                .put("downloadUrlAvailability", getYesNoPercentage())
                .put("downloadUrlStatusCodes", statusCodes);

        JsonObject interoperability = new JsonObject()
                .put("formatAvailability", getYesNoPercentage())
                .put("mediaTypeAvailability", getYesNoPercentage())
                .put("formatMediaTypeAlignment", getYesNoPercentage())
                .put("formatMediaTypeNonProprietary", getYesNoPercentage())
                .put("formatMediaTypeMachineReadable", getYesNoPercentage())
                .put("dcatApCompliance", getYesNoPercentage());

        JsonObject reusability = new JsonObject()
                .put("licenceAvailability", getYesNoPercentage())
                .put("licenceAlignment", getYesNoPercentage())
                .put("accessRightsAvailability", getYesNoPercentage())
                .put("accessRightsAlignment", getYesNoPercentage())
                .put("contactPointAvailability", getYesNoPercentage())
                .put("publisherAvailability", getYesNoPercentage());

        JsonObject contextuality = new JsonObject()
                .put("rightsAvailability", getYesNoPercentage())
                .put("byteSizeAvailability", getYesNoPercentage())
                .put("dateIssuedAvailability", getYesNoPercentage())
                .put("dateModifiedAvailability", getYesNoPercentage());

        return new JsonObject()
                .put("findability", findability)
                .put("accessibility", accessibility)
                .put("interoperability", interoperability)
                .put("reusability", reusability)
                .put("contextuality", contextuality)
                .put("score", ThreadLocalRandom.current().nextInt(0, 405));
    }

    private JsonArray getYesNoPercentage() {
        double percentage = ThreadLocalRandom.current().nextDouble(0, 100);
        return new JsonArray()
                .add(new JsonObject().put("name", "yes").put("percentage", percentage))
                .add(new JsonObject().put("name", "no").put("percentage", 100 - percentage));
    }

    private JsonObject generateInfo() {
        JsonObject accessibility = new JsonObject()
                .put("accessUrlStatusCodes", testMetrics.getJsonObject("accessibility").getJsonArray("accessUrlStatusCodes"))
                .put("downloadUrlStatusCodes", testMetrics.getJsonObject("accessibility").getJsonArray("downloadUrlStatusCodes"));

        JsonObject interoperability = new JsonObject()
                .put("formatMediaTypeMachineReadable", testMetrics.getJsonObject("interoperability").getJsonArray("formatMediaTypeMachineReadable"))
                .put("dcatApCompliance", testMetrics.getJsonObject("interoperability").getJsonArray("dcatApCompliance"));

        return new JsonObject()
                .put("info", getInfoObject())
                .put("score", testMetrics.getDouble("score"))
                .put("accessibility", accessibility)
                .put("interoperability", interoperability);


    }

    private JsonObject getInfoObject() {

        return new JsonObject()
                .put("id", "testCatalogue")
                .put("title", "Test Catalogue")
                .put("description", "This is a Test Catalogue")
                .put("spatial", "DEU").put("type", "dcat-ap");
    }


}
