package io.piveau.metrics.cache;

import io.piveau.dcatap.DCATAPUriSchema;
import io.piveau.json.ConfigHelper;
import io.piveau.metrics.cache.dqv.DqvProvider;
import io.piveau.metrics.cache.dqv.DqvVerticle;
import io.piveau.metrics.cache.dqv.sparql.QueryCollection;
import io.piveau.metrics.cache.persistence.DatabaseProvider;
import io.piveau.metrics.cache.persistence.DatabaseVerticle;
import io.piveau.metrics.cache.persistence.DocumentScope;
import io.vertx.config.ConfigRetriever;
import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.api.contract.RouterFactoryOptions;
import io.vertx.ext.web.api.contract.openapi3.OpenAPI3RouterFactory;
import io.vertx.ext.web.handler.CorsHandler;
import io.vertx.ext.web.handler.StaticHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


public class MainVerticle extends AbstractVerticle {

    private static final Logger log = LoggerFactory.getLogger(MainVerticle.class);

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private JsonObject config;
    private ApiKeyHandler apiKeyHandler;
    private DatabaseProvider databaseProvider;
    private DqvProvider dqvProvider;

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

    @Override
    public void start(Promise<Void> startPromise) {
        log.info("Launching Cache...");

        QueryCollection.init(vertx, "queries");

        // startup is only successful if no step failed
        loadConfig()
                .compose(handler -> bootstrapVerticles())
                .compose(handler -> startServer())
                .onComplete(startPromise);
    }

    private Future<Void> loadConfig() {
        return Future.future(loadConfig -> {
            ConfigRetriever configRetriever = ConfigRetriever.create(vertx);

            configRetriever.getConfig(handler -> {
                if (handler.succeeded()) {
                    config = handler.result();
                    DCATAPUriSchema.INSTANCE.setConfig(new JsonObject().put("baseUri", config.getString(ApplicationConfig.ENV_BASE_URI, ApplicationConfig.DEFAULT_BASE_URI)));

                    log.debug(config.encodePrettily());
                    loadConfig.complete();
                } else {
                    loadConfig.fail("Failed to load config: " + handler.cause());
                }
            });
        });
    }

    private CompositeFuture bootstrapVerticles() {
        DeploymentOptions options = new DeploymentOptions()
                .setConfig(config)
                .setWorker(true);

        List<Future<Void>> deploymentFutures = new ArrayList<>();
        deploymentFutures.add(startVerticle(new DeploymentOptions()
                .setConfig(ConfigHelper.forConfig(config).forceJsonObject("PIVEAU_TRIPLESTORE_CONFIG"))
                .setWorker(true), DqvVerticle.class));

        deploymentFutures.add(startVerticle(options, DatabaseVerticle.class));

        return CompositeFuture.join(new ArrayList<>(deploymentFutures));
    }

    private Future<Void> startVerticle(DeploymentOptions options, Class<? extends AbstractVerticle> clazz) {
        return Future.future(startVerticle -> vertx.deployVerticle(clazz.getName(), options, handler -> {
            if (handler.succeeded()) {
                startVerticle.complete();
            } else {
                startVerticle.fail("Failed to deploy [" + clazz.getName() + "] : " + handler.cause());
            }
        }));
    }

    private Future<Void> startServer() {
        return Future.future(startServer -> {
            databaseProvider = DatabaseProvider.createProxy(vertx, DatabaseProvider.SERVICE_ADDRESS);
            Integer port = config.getInteger(ApplicationConfig.ENV_APPLICATION_PORT, ApplicationConfig.DEFAULT_APPLICATION_PORT);
//FIXME
            DeliveryOptions options1 = new DeliveryOptions().setSendTimeout(3000000);
            dqvProvider = DqvProvider.createProxy(vertx, DqvProvider.SERVICE_ADDRESS, options1);

            OpenAPI3RouterFactory.create(vertx, "webroot/openapi.yaml", handler -> {
                if (handler.succeeded()) {

                    OpenAPI3RouterFactory routerFactory = handler.result();
                    RouterFactoryOptions options = new RouterFactoryOptions().setMountNotImplementedHandler(true);
                    routerFactory.setOptions(options);

                    JsonArray corsDomains = ConfigHelper.forConfig(config).forceJsonArray(ApplicationConfig.ENV_CACHE_CORS_DOMAINS);
                    if (!corsDomains.isEmpty()) {

                        Set<String> allowedHeaders = new HashSet<>();
                        allowedHeaders.add("x-requested-with");
                        allowedHeaders.add("Access-Control-Allow-Origin");
                        allowedHeaders.add("origin");
                        allowedHeaders.add("Content-Type");
                        allowedHeaders.add("accept");
                        allowedHeaders.add("Authorization");

                        Set<HttpMethod> allowedMethods = new HashSet<>();
                        allowedMethods.add(HttpMethod.GET);
                        allowedMethods.add(HttpMethod.POST);
                        allowedMethods.add(HttpMethod.OPTIONS);
                        allowedMethods.add(HttpMethod.DELETE);
                        allowedMethods.add(HttpMethod.PATCH);
                        allowedMethods.add(HttpMethod.PUT);

                        ArrayList<String> corsArray = new ArrayList<>();
                        for (int i = 0; i < corsDomains.size(); i++) {
                            //convert into normal array and escape dots for regex compatiility
                            corsArray.add(corsDomains.getString(i).replace(".", "\\."));
                        }

                        //"^(https?:\\/\\/(?:.+\\.)?(?:fokus\\.fraunhofer\\.de|localhost)(?::\\d{1,5})?)$"
                        String corsString = "^(https?:\\/\\/(?:.+\\.)?(?:" + String.join("|", corsArray) + ")(?::\\d{1,5})?)$";
                        routerFactory.addGlobalHandler(CorsHandler.create(corsString).allowedHeaders(allowedHeaders).allowedMethods(allowedMethods).allowCredentials(true));
                    } else {
                        log.info("no CORS domains configured");
                    }


                    apiKeyHandler = new ApiKeyHandler(config.getString(ApplicationConfig.ENV_APIKEY));
                    routerFactory.addSecurityHandler("ApiKeyAuth", apiKeyHandler::checkApiKey);

                    routerFactory
                            // Administration
                            .addHandlerByOperationId("refreshMetrics", this::refreshMetrics)
                            .addHandlerByOperationId("refreshSingleMetrics", this::refreshSingleMetrics)
                            .addHandlerByOperationId("clearMetrics", this::clearMetrics)

                            // Global
                            .addHandlerByOperationId("getGlobalMetrics", context -> getDocument(DocumentScope.GLOBAL, "global", context))

                            // Countries
                            .addHandlerByOperationId("getCountryMetrics", context -> fetch(DocumentScope.COUNTRY, context))

                            // Catalogues
                            .addHandlerByOperationId("listCatalogueSummaries", this::getInfos)
                            .addHandlerByOperationId("getCatalogueMetrics", context -> fetch(DocumentScope.CATALOGUE, context))
                            .addHandlerByOperationId("deleteCatalogueMetrics", this::deleteCatalogueMetrics)
                            .addHandlerByOperationId("getCatalogueDistributionReachability", this::getCatalogueDistributionReachability)
                            .addHandlerByOperationId("getCatalogueViolations", this::getViolations)

                            // Global, Countries, Catalogues
                            .addHandlerByOperationId("getScores", this::getScore);

                    Router router = routerFactory.getRouter();
                    router.route("/*").handler(StaticHandler.create());

                    HttpServer server = vertx.createHttpServer(new HttpServerOptions().setPort(port));
                    server.requestHandler(router).listen();

                    log.info("Server successfully launched on port [{}]", port);
                    startServer.complete();
                } else {
                    // Something went wrong during router factory initialization
                    log.error("Failed to start server at [{}]: {}", port, handler.cause());
                    startServer.fail(handler.cause());
                }
            });
        });
    }

    private void fetch(DocumentScope documentScope, RoutingContext context) {
        String id = context.pathParam("id");

        if (id == null || id.isEmpty()) {
            listDocuments(documentScope, context);
        } else {
            getDocument(documentScope, id, context);
        }
    }

    private void deleteCatalogueMetrics(RoutingContext context) {
        databaseProvider.deleteDocument(DocumentScope.CATALOGUE, context.pathParam("id"), ar -> {
            if (ar.succeeded()) {
                String result = ar.result();
                if (result.equals("success")) {
                    context.response().setStatusCode(200).end();
                } else if (result.equals("not found")) {
                    context.response().setStatusCode(404).end();
                } else {
                    context.response().setStatusCode(400).setStatusMessage(result).end();
                }
            } else {
                context.response().setStatusCode(500).end(ar.cause().getMessage());
            }
        });
    }

    private void getInfos(RoutingContext context) {
        databaseProvider.getInfos(DocumentScope.CATALOGUE, list -> {
            if (list.succeeded()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(list.result().encodePrettily());
            } else {
                context.response().setStatusCode(500).end();
            }
        });
    }

    private void getScore(RoutingContext context) {
        log.trace("getScore called");
        String id = context.pathParam("id");

        MultiMap queryParams = context.queryParams();
        String resolution = queryParams.contains("resolution") ? queryParams.get("resolution") : "month";

        String startDate = queryParams.get("startDate");
        String endDate = queryParams.contains("endDate") ? queryParams.get("endDate") : dateFormat.format(new Date());

        try {
            if (dateFormat.parse(startDate).after(dateFormat.parse(endDate))) {
                context.response().setStatusCode(400).setStatusMessage("Bad Request - startDate after endDate").end();
                return;
            }
        } catch (ParseException e) {
            context.response().setStatusCode(400).end();
            return;
        }

        databaseProvider.getScore(resolution, startDate, endDate, id, ar -> {
            if (ar.succeeded()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(ar.result().encodePrettily());
            } else {
                log.error("Get score failed", ar.cause());
                context.response().setStatusCode(500).end();
            }
        });
    }

    private void listDocuments(DocumentScope documentScope, RoutingContext context) {
        databaseProvider.listDocuments(documentScope, list -> {
            if (list.succeeded()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(new JsonArray(list.result()).encodePrettily());
            } else {
                context.response().setStatusCode(500).end();
            }
        });
    }

    private void getDocument(DocumentScope documentScope, String id, RoutingContext context) {
        databaseProvider.getDocument(documentScope, id, fetch -> {
            if (fetch.succeeded()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(fetch.result().encodePrettily());
            } else {
                context.response().setStatusCode(404).end();
            }
        });
    }

    private void refreshMetrics(RoutingContext context) {
        databaseProvider.refreshMetrics();
        context.response().setStatusCode(202).end();
    }

    private void refreshSingleMetrics(RoutingContext context) {
        databaseProvider.refreshSingleMetrics(context.pathParam("id"));
        context.response().setStatusCode(202).end();
    }

    private void clearMetrics(RoutingContext context) {
        databaseProvider.clearMetrics();
        context.response().setStatusCode(202).end();
    }

    /**
     * getViolations method is opertaion connected to endpoint  /metrics/catalogues/<cataloug>/violation
     *
     * @param context the RoutingContext
     */
    private void getViolations(RoutingContext context) {
        String id = context.pathParam("id");

        MultiMap params = context.queryParams();
        String lang = params.contains("locale") ? params.get("locale") : "en";

        int offset = params.contains("offset") ? Integer.parseInt(params.get("offset")) : 0;
        int limit = params.contains("limit") ? Integer.parseInt(params.get("limit")) : 100;

        dqvProvider.getCatalogueViolations(id, offset, limit, lang, ar -> {
            if (ar.succeeded()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(ar.result().encodePrettily());
            } else {
                log.error("Failed to get violations", ar.cause());
                context.response().setStatusCode(500).end();
            }
        });
    }

    /**
     * get error status codes method get error status codes for all distributions of a dataset for a specific catalogue.
     * and put it JSON structure for the response:
     * {
     * "success": true,
     * "result": {
     * "count": 47,
     * "results": [
     * {
     *
     * @param context the routing context
     */
    private void getCatalogueDistributionReachability(RoutingContext context) {
        String id = context.pathParam("id");

        MultiMap params = context.queryParams();
        String lang = params.contains("locale") ? params.get("locale") : "en";

        int offset = params.contains("offset") ? Integer.parseInt(params.get("offset")) : 0;
        int limit = params.contains("limit") ? Integer.parseInt(params.get("limit")) : 100;

        databaseProvider.getDistributionReachabilityDetails(id, offset, limit, lang, ar -> {
            if (ar.succeeded()) {
                context.response()
                        .setStatusCode(200)
                        .putHeader("Content-Type", "application/json")
                        .end(ar.result().encodePrettily());
            } else {
                log.error("Failed to get reachability issues", ar.cause());
                // differentiate: e.g. id not found -> 404
                context.response().setStatusCode(500).end();
            }
        });
    }

}
