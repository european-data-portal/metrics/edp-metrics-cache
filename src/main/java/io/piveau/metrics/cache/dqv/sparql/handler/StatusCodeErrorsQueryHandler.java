package io.piveau.metrics.cache.dqv.sparql.handler;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import org.apache.jena.query.ResultSet;

public class StatusCodeErrorsQueryHandler extends QueryHandler {

    public StatusCodeErrorsQueryHandler(String catalogueGraphName, String catalogueUriRef, int offset, int limit) {
        super("StatusCodeErrors", catalogueGraphName, catalogueUriRef, offset, limit);
        result.put("results", new JsonArray());
    }

    @Override
    public void handle(ResultSet resultSet) {

        resultSet.forEachRemaining(solution -> {
            JsonObject dataset = new JsonObject()
                    .put("reference", solution.getResource("ds").getURI())
                    .put("distribution", solution.getResource("dist").getURI());

            //download URL
            if (solution.contains("downloadUrl")) {
                dataset.put("downloadUrl", solution.getResource("downloadUrl").getURI());
            }

            //download Url status code
            if (solution.contains("downloadUrlStatusCode")) {
                dataset.put("downloadUrlStatusCode", solution.getLiteral("downloadUrlStatusCode").getInt());
            }

            //timeStamp
            if (solution.contains("time2")) {
                dataset.put("downloadUrlTimeStamp", solution.getLiteral("time2").getLexicalForm());
            }

            //access URL
            if (solution.contains("accessUrl")) {
                dataset.put("accessUrl", solution.getResource("accessUrl").getURI());
            }

            //access Url status code
            if (solution.contains("accessUrlStatusCode")) {
                dataset.put("accessUrlStatusCode", solution.getLiteral("accessUrlStatusCode").getInt());
            }

            //timeStamp
            if (solution.contains("time1")) {
                dataset.put("accessUrlTimeStamp", solution.getLiteral("time1").getLexicalForm());
            }

            result.getJsonArray("results").add(dataset);
        });
    }

}
