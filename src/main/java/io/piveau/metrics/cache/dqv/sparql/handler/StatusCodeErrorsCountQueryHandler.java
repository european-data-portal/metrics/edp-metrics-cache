package io.piveau.metrics.cache.dqv.sparql.handler;

import org.apache.jena.query.ResultSet;

public class StatusCodeErrorsCountQueryHandler extends QueryHandler {

    public StatusCodeErrorsCountQueryHandler(String catalogueGraphName, String catalogueUriRef) {
        super("StatusCodeErrorsCount", catalogueGraphName, catalogueUriRef);
    }

    @Override
    public void handle(ResultSet resultSet) {
        if (resultSet.hasNext()) {
            result.put("count", resultSet.next().getLiteral("count").getInt());
        }
    }

}
