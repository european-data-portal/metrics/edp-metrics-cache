package io.piveau.metrics.cache.persistence;

import io.piveau.log.PiveauLogger;
import io.piveau.metrics.cache.dqv.DqvProvider;
import io.piveau.metrics.cache.dqv.StatusCodes;
import io.piveau.metrics.cache.dqv.sparql.util.SparqlHelper;
import io.piveau.utils.PiveauContext;
import io.vertx.core.*;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static io.piveau.metrics.cache.ApplicationConfig.*;

public class DatabaseProviderImpl implements DatabaseProvider {

    private static final Logger log = LoggerFactory.getLogger(DatabaseProviderImpl.class);

    private final DqvProvider dqvProvider;
    private final MongoClient dbClient;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private final PiveauLogger piveauLog = new PiveauContext("metrics-cache", "cache").log();

    public DatabaseProviderImpl(Vertx vertx, Handler<AsyncResult<DatabaseProvider>> readyHandler) {

        JsonObject env = vertx.getOrCreateContext().config();

        /*
         * connection to mongoDB is established twice which results in this weird authentication error
         */

        JsonObject config = new JsonObject()
                .put("serverSelectionTimeoutMS", 1000)
                .put("host", env.getString(ENV_MONGODB_SERVER_HOST, DEFAULT_MONGODB_SERVER_HOST))
                .put("port", env.getInteger(ENV_MONGODB_SERVER_PORT, DEFAULT_MONGODB_SERVER_PORT))
                .put("db_name", env.getString(ENV_MONGODB_DB_NAME, DEFAULT_MONGODB_DB_NAME))
                .put("username", env.getString(ENV_MONGODB_USERNAME, DEFAULT_MONGODB_USERNAME))
                .put("password", env.getString(ENV_MONGODB_PASSWORD, DEFAULT_MONGODB_PASSWORD));


        log.info("MongoDB config: {}", config);
        dbClient = MongoClient.createShared(vertx, config);
        DeliveryOptions options = new DeliveryOptions().setSendTimeout(3000000);
        dqvProvider = DqvProvider.createProxy(vertx, DqvProvider.SERVICE_ADDRESS, options);

        // check if database connection can be established
        dbClient.getCollections(collections -> {
            if (collections.succeeded()) {
                readyHandler.handle(Future.succeededFuture(this));
            } else {
                log.debug(collections.cause().toString());
                readyHandler.handle(Future.failedFuture("Failed to establish database connection"));
            }
        });
    }

    @Override
    public void listDocuments(DocumentScope documentScope, Handler<AsyncResult<List<String>>> resultHandler) {
        FindOptions fo = new FindOptions();
        fo.setFields(new JsonObject()); // _id is alway returned in response objects, so empty json as filter is enough, no additional fields needed
        dbClient.findWithOptions(documentScope.name(), new JsonObject(), fo, handler -> {
            if (handler.succeeded()) {
                List<String> documentIds = handler.result().stream()
                        .map(json -> json.getString("_id"))
                        .collect(Collectors.toList());

                resultHandler.handle(Future.succeededFuture(documentIds));
            } else {
                resultHandler.handle(Future.failedFuture(handler.cause()));
            }
        });
    }


    @Override
    public void getInfos(DocumentScope documentScope, Handler<AsyncResult<JsonArray>> resultHandler) {
        JsonObject fields = new JsonObject();
        fields.put("info", 1).put("score", 1)
                .put("interoperability.formatMediaTypeMachineReadable", 1)
                .put("interoperability.dcatApCompliance", 1)
                .put("accessibility.accessUrlStatusCodes", 1)
                .put("accessibility.downloadUrlStatusCodes", 1);
        FindOptions fo = new FindOptions();
        fo.setFields(fields);
        dbClient.findWithOptions(documentScope.name(), new JsonObject(), fo, handler -> {
            if (handler.succeeded()) {

                JsonArray documents = new JsonArray();
                handler.result().forEach(jsonObject -> {
                    jsonObject.remove("_id");
                    documents.add(jsonObject);
                });

                resultHandler.handle(Future.succeededFuture(documents));
            } else {
                resultHandler.handle(Future.failedFuture(handler.cause()));
            }
        });
    }

    @Override
    public void getDocument(DocumentScope documentScope, String id, Handler<AsyncResult<JsonObject>> resultHandler) {
        JsonObject metricId = new JsonObject().put("_id", id);
        //log.debug("Attempting to retrieve metric with ID [{}]", metricId);

        dbClient.findOne(documentScope.name(), metricId, null, ar -> {
            if (ar.succeeded()) {
                if (ar.result() != null) {
                    JsonObject result = ar.result();
                    result.remove("_id"); // internal mongo ID is irrelevant to user
                    resultHandler.handle(Future.succeededFuture(result));
                } else {
                    resultHandler.handle(Future.failedFuture("No results found"));
                }
            } else {
                resultHandler.handle(Future.failedFuture("No results found"));
            }
        });
    }

    @Override
    public void deleteDocument(DocumentScope documentScope, String id, Handler<AsyncResult<String>> resultHandler) {
        JsonObject filter = new JsonObject().put("_id", id);
        dbClient.removeDocument(documentScope.name(), filter, cr -> {
            if (cr.succeeded()) {
                dbClient.removeDocument(DocumentScope.SCORE.name(), filter, sr -> {
                    if (sr.succeeded()) {
                        resultHandler.handle(Future.succeededFuture("success"));
                    } else {
                        resultHandler.handle(Future.failedFuture(sr.cause()));
                    }
                });
            } else {
                resultHandler.handle(Future.failedFuture(cr.cause()));
            }
        });
    }

    @Override
    public void getScore(String resolution, String startDate, String endDate, String id, Handler<AsyncResult<JsonObject>> resultHandler) {

        dbClient.findOne(DocumentScope.SCORE.name(), new JsonObject().put("_id", id), null, ar -> {
            if (ar.succeeded() && ar.result() != null) {
                JsonArray scores = ar.result().getJsonArray("scores");

                List<JsonObject> dates = new ArrayList<>();

                scores.forEach(score -> {
                    JsonObject scoreObj = (JsonObject) score;
                    dates.add(scoreObj);
                });

                /*We parse the dates again because they need to be strings on the eventbus*/

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate startdate;
                LocalDate enddate;

                startdate = LocalDate.parse(startDate, formatter);
                enddate = LocalDate.parse(endDate, formatter);

                LocalDate finalStartdate = startdate;
                LocalDate finalEnddate = enddate;

                List<JsonObject> finalCuttedScores = new ArrayList<>(dates);

                /*we cut the data in the provided dates everything else is removed*/

                dates.forEach(score -> {
                    LocalDate date;
                    date = LocalDate.parse(score.getString("date"), formatter);
                    if (date.isBefore(finalStartdate) || date.isAfter(finalEnddate))
                        finalCuttedScores.remove(score);
                });

                JsonArray finalResult = new JsonArray();
                HashMap<String, ArrayList<Double>> counter = new HashMap<>();

/*
                depending of resolution we calculate the average depending of dates as discussed in the daily
*/
                switch (resolution) {
                    case "year":
                        for (JsonObject entries : finalCuttedScores) {
                            Map<String, ArrayList<Double>> cloneCounter = new HashMap<>(counter);
                            String date = entries.getString("date");
                            String[] splitDate = date.split("[-]");

                            /*check if the date is already in the array if not create a new list with scores else copy and replace the existing one*/

                            if (cloneCounter.containsKey((splitDate[0]))) {
                                ArrayList<Double> values = counter.get((splitDate[0]));
                                values.add(entries.getDouble("score"));
                                counter.replace((splitDate[0]), values);
                            } else {
                                ArrayList<Double> newList = new ArrayList<>();
                                newList.add(entries.getDouble("score"));
                                counter.put((splitDate[0]), newList);
                            }
                        }

                        /*we remove all dates which are not years */

                        counter.keySet().forEach(key -> {
                            if (key.length() > 4)
                                counter.remove(key);
                        });
                        break;
                    case "day":

                        for (JsonObject finalCuttedScore : finalCuttedScores) {
                            Map<String, ArrayList<Double>> cloneCounter = new HashMap<>(counter);

                            /*check if the date is already in the array if not create a new list with scores else copy and replace the existing one*/

                            String date = finalCuttedScore.getString("date");
                            if (cloneCounter.containsKey(date)) {
                                ArrayList<Double> values = counter.get(date);
                                values.add(finalCuttedScore.getDouble("score"));
                                counter.replace(date, values);
                            } else {
                                ArrayList<Double> newList = new ArrayList<>();
                                newList.add(finalCuttedScore.getDouble("score"));
                                counter.put(date, newList);
                            }
                        }
                        break;
                    case "month":
                        for (JsonObject score : finalCuttedScores) {
                            Map<String, ArrayList<Double>> cloneCounter = new HashMap<>(counter);
                            String date = score.getString("date");
                            String[] splitDate = date.split("[-]");

                            /*check if the date is already in the array if not create a new list with scores else copy and replace the existing one*/

                            if (cloneCounter.containsKey((splitDate[0] + "-" + splitDate[1]))) {
                                ArrayList<Double> values = counter.get((splitDate[0] + "-" + splitDate[1]));
                                values.add(score.getDouble("score"));
                                counter.replace((splitDate[0] + "-" + splitDate[1]), values);
                            } else {
                                ArrayList<Double> newList = new ArrayList<>();
                                newList.add(score.getDouble("score"));
                                counter.put((splitDate[0] + "-" + splitDate[1]), newList);
                            }
                        }

                        /*we delete all entries which are not months*/

                        counter.keySet().forEach(key -> {
                            if (key.length() > 8)
                                counter.remove(key);
                        });
                        break;
                    default:
                        log.error("Unexpected value as resolution: " + resolution);
                        resultHandler.handle(Future.failedFuture("400 - Unexpected value as resolution: " + resolution));
                        throw new IllegalStateException("Unexpected value: " + resolution);
                }

                /*in order to sort we add the keys/dates to a list */

                List<String> listToStore = new ArrayList<>(counter.keySet());

                /*sort it*/

                Collections.sort(listToStore);

                /*now we build our final result which shows the averages and is sorted*/

                listToStore.forEach(key -> {
                    ArrayList<Double> list = counter.get(key);
                    double total = list.stream().mapToDouble(score -> score).sum();
                    finalResult.add(new JsonObject().put(key, total / list.size()));
                });

                resultHandler.handle(Future.succeededFuture(new JsonObject().put("Result", finalResult)));

            } else {
                if (id.equals("global")) {
                    log.debug("No global score available");
                    resultHandler.handle(Future.succeededFuture(new JsonObject().put("Result", "No global score available")));
                } else {
                    resultHandler.handle(Future.succeededFuture(new JsonObject().put("Result", "No score available")));
                }
            }
        });

    }

    @Override
    public void refreshMetrics() {
        piveauLog.info("Start refreshing cache");
        long start = System.currentTimeMillis();
        dqvProvider.listCatalogues(ar -> {
            if (ar.succeeded()) {
                piveauLog.info("Found {} catalogues", ar.result().size());
                collectMetrics(DocumentScope.CATALOGUE, ar.result()).compose(v -> {
                    piveauLog.info("Catalogues refreshed ({}), start refreshing global", System.currentTimeMillis() - start);
                    return collectMetrics(DocumentScope.GLOBAL, Collections.singletonList("global"));
                }).compose(v -> {
                    piveauLog.info("Global refreshed ({}), start refreshing countries", System.currentTimeMillis() - start);
                    return collectMetrics(DocumentScope.COUNTRY, new ArrayList<>(SparqlHelper.getCountries().keySet()));
                }).onComplete(v -> piveauLog.info("Cache refresh finished ({} ms)", System.currentTimeMillis() - start));
            } else {
                piveauLog.error("Get catalogue list", ar.cause());
            }
        });
    }

    @Override
    public void refreshSingleMetrics(String id) {
        // For now we assume always it is a catalogue
        // Needs to be extended
        // Also no 404 response is send in case id does not exist
        collectMetrics(DocumentScope.CATALOGUE, Collections.singletonList(id));
    }

    @Override
    public void clearMetrics() {
        piveauLog.info("Start clearing metrics");
        for (DocumentScope metric : DocumentScope.values()) {
            dbClient.dropCollection(metric.name(), ar -> {
                if (ar.succeeded()) {
                    piveauLog.info("All metrics dropped");
                } else {
                    piveauLog.error("Dropping metrics failed", ar.cause());
                }
            });
        }
    }

    @Override
    public void getDistributionReachabilityDetails(String catalogueId, int offset, int limit, String lang, Handler<AsyncResult<JsonObject>> resultHandler) {
        Promise<JsonObject> statusCodes = Promise.promise();
        dqvProvider.getDistributionReachabilityDetails(catalogueId, offset, limit, lang, statusCodes);
        statusCodes.future().onComplete(retrieveStatusCodesList -> {
            if (retrieveStatusCodesList.succeeded()) {
                JsonObject document = new JsonObject().put("success", "true");
                document.put("result", retrieveStatusCodesList.result());
                resultHandler.handle(Future.succeededFuture(document));
            } else {
                resultHandler.handle(Future.failedFuture(retrieveStatusCodesList.cause()));
            }
        });
    }

    private Future<Void> collectMetrics(DocumentScope scope, List<String> collection) {
        Promise<Void> promise = Promise.promise();
        Iterator<String> iterator = collection.iterator();
        collectIterative(iterator, scope, promise);
        return promise.future();
    }

    private void collectIterative(Iterator<String> iterator, DocumentScope scope, Promise<Void> finishedPromise) {
        if (iterator.hasNext()) {
            String next = iterator.next();
            collectMetricsForID(scope, next)
                    .onSuccess(v -> collectIterative(iterator, scope, finishedPromise))
                    .onFailure(finishedPromise::fail);
        } else {
            finishedPromise.complete();
        }
    }

    private Future<Void> collectMetricsForID(DocumentScope scope, String documentId) {
        Promise<Void> promise = Promise.promise();

        JsonObject document = new JsonObject().put("_id", documentId);
        addFindability(document, scope).compose(v ->
                addAccessibility(document, scope)
        ).compose(v ->
                addInteroperability(document, scope)
        ).compose(v ->
                addReusability(document, scope)
        ).compose(result ->
                addContextuality(document, scope)
        ).compose(v ->
            Future.<Double>future(pr -> dqvProvider.getAverageScore(documentId, scope, pr))
        ).compose(result -> {
            document.put("score", result);
            saveScoreToTimeline(documentId, result);

            return Future.<Void>future(pr -> {
                if (scope == DocumentScope.CATALOGUE) {
                    Promise<JsonObject> infoPromise = Promise.promise();
                    dqvProvider.getCatalogueInfo(documentId, infoPromise);
                    infoPromise.future().onSuccess(info -> {
                        document.put("info", info);
                        pr.complete();
                    }).onFailure(pr::fail);
                } else {
                    pr.complete();
                }
            });
        }).onSuccess(v ->
                dbClient.save(scope.name(), document, ar -> {
                    if (ar.succeeded()) {
                        log.info("Saved metrics for document {}", documentId);
                    } else {
                        log.error("Failed to save metrics for document ID {}", documentId, ar.cause());
                    }
                    promise.complete();
                })
        ).onFailure(cause -> {
            log.error("Collecting metrics for {} failed", documentId, cause);
            promise.complete();
        });

        return promise.future();
    }

    private void saveScoreToTimeline(String documentId, Double doubleScore) {
        JsonObject query = new JsonObject().put("_id", documentId);
        dbClient.findOne(DocumentScope.SCORE.name(), query, null, ar -> {
            if (ar.succeeded()) {
                JsonObject newScore = new JsonObject()
                        .put("score", doubleScore)
                        .put("date", dateFormat.format(new Date()));

                JsonObject scoreList = ar.result() != null ? ar.result() : new JsonObject();
                JsonArray scores = scoreList.containsKey("scores") ? scoreList.getJsonArray("scores") : new JsonArray();
                scores.add(newScore);

                scoreList.put("scores", scores);
                scoreList.put("_id", documentId);

                if (ar.result() != null) {
                    dbClient.replaceDocuments(DocumentScope.SCORE.name(), query, scoreList, sr -> {
                        if (sr.succeeded()) {
                            log.info("Saved score timeline for {}", documentId);
                        } else {
                            log.error("Error saving the timeline", sr.cause());
                        }
                    });
                } else {
                    dbClient.save(DocumentScope.SCORE.name(), scoreList, sr -> {
                        if (sr.succeeded()) {
                            log.info("Saved score timeline for {}", documentId);
                        } else {
                            log.error("Error saving the timeline", sr.cause());
                        }
                    });
                }
            } else {
                log.error("Error saving timeline for {}", documentId, ar.cause());
            }
        });
    }

    private Future<Void> addFindability(JsonObject document, DocumentScope documentScope) {
        Promise<Void> promise = Promise.promise();

        JsonObject findability = new JsonObject();
        document.put("findability", findability);

        String catalogueId = document.getString("_id");

        Promise<Double> keywordAvailability = Promise.promise();
        dqvProvider.getKeywordAvailability(catalogueId, documentScope, keywordAvailability);
        keywordAvailability.future().compose(availability -> {
            if (availability != -1.0) {
                findability.put("keywordAvailability", getYesNoPercentage(availability));
            }
            return Future.<Double>future(pr -> dqvProvider.getCategoryAvailability(catalogueId, documentScope, pr));
        }).compose(availability -> {
            if (availability != -1.0) {
                findability.put("categoryAvailability", getYesNoPercentage(availability));
            }
            return Future.<Double>future(pr -> dqvProvider.getSpatialAvailability(catalogueId, documentScope, pr));
        }).compose(availability -> {
            if (availability != -1.0) {
                findability.put("spatialAvailability", getYesNoPercentage(availability));
            }
            return Future.<Double>future(pr -> dqvProvider.getTemporalAvailability(catalogueId, documentScope, pr));
        }).onSuccess(availability -> {
            if (availability != -1.0) {
                findability.put("temporalAvailability", getYesNoPercentage(availability));
            }
            promise.complete();
        }).onFailure(promise::fail);

        return promise.future();
    }

    private Future<Void> addAccessibility(JsonObject document, DocumentScope documentScope) {
        Promise<Void> promise = Promise.promise();

        JsonObject accessibility = new JsonObject();
        document.put("accessibility", accessibility);

        String catalogueId = document.getString("_id");

        Promise<StatusCodes> accessUrlStatusCodes = Promise.promise();
        dqvProvider.getAccessUrlStatusCodes(catalogueId, documentScope, accessUrlStatusCodes);
        accessUrlStatusCodes.future().compose(statusCodes -> {
            JsonArray accessUrlStatusCodeArray = new JsonArray();
            statusCodes.getStatusCodes().forEach((code, percentage) ->
                    accessUrlStatusCodeArray.add(new JsonObject().put("name", code).put("percentage", percentage)));
            accessibility.put("accessUrlStatusCodes", accessUrlStatusCodeArray);

            return Future.<StatusCodes>future(pr -> dqvProvider.getDownloadUrlStatusCodes(catalogueId, documentScope, pr));
        }).compose(statusCodes -> {
            JsonArray downloadUrlStatusCodeArray = new JsonArray();
            statusCodes.getStatusCodes().forEach((code, percentage) ->
                    downloadUrlStatusCodeArray.add(new JsonObject().put("name", code).put("percentage", percentage)));
            accessibility.put("downloadUrlStatusCodes", downloadUrlStatusCodeArray);

            return Future.<Double>future(pr -> dqvProvider.getDownloadUrlAvailability(catalogueId, documentScope, pr));
        }).onSuccess(availability -> {
            if (availability != -1.0) {
                accessibility.put("downloadUrlAvailability", getYesNoPercentage(availability));
            }
            promise.complete();
        }).onFailure(promise::fail);

        return promise.future();
    }

    private Future<Void> addInteroperability(JsonObject document, DocumentScope documentScope) {
        Promise<Void> promise = Promise.promise();

        JsonObject interoperability = new JsonObject();
        document.put("interoperability", interoperability);

        String catalogueId = document.getString("_id");

        Promise<Double> formatAvailability = Promise.promise();
        dqvProvider.getFormatAvailability(catalogueId, documentScope, formatAvailability);
        formatAvailability.future().compose(result -> {
            if (result != -1.0) {
                interoperability.put("formatAvailability", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getMediaTypeAvailability(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                interoperability.put("mediaTypeAvailability", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getFormatMediaTypeAlignment(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                interoperability.put("formatMediaTypeAlignment", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getFormatMediaTypeNonProprietary(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                interoperability.put("formatMediaTypeNonProprietary", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getFormatMediaTypeMachineReadable(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                interoperability.put("formatMediaTypeMachineReadable", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getDcatApCompliance(catalogueId, documentScope, pr));
        }).onSuccess(result -> {
            if (result != -1.0) {
                interoperability.put("dcatApCompliance", getYesNoPercentage(result));
            }
            promise.complete();
        }).onFailure(promise::fail);

        return promise.future();
    }

    private Future<Void> addReusability(JsonObject document, DocumentScope documentScope) {
        Promise<Void> promise = Promise.promise();

        JsonObject reusability = new JsonObject();
        document.put("reusability", reusability);

        String catalogueId = document.getString("_id");

        Promise<Double> licenceAvailability = Promise.promise();
        dqvProvider.getLicenceAvailability(catalogueId, documentScope, licenceAvailability);
        licenceAvailability.future().compose(result -> {
            if (result != -1.0) {
                reusability.put("licenceAvailability", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getLicenceAlignment(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                reusability.put("licenceAlignment", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getAccessRightsAvailability(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                reusability.put("accessRightsAvailability", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getAccessRightsAlignment(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                reusability.put("accessRightsAlignment", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getContactPointAvailability(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                reusability.put("contactPointAvailability", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getPublisherAvailability(catalogueId, documentScope, pr));
        }).onSuccess(result -> {
            if (result != -1.0) {
                reusability.put("publisherAvailability", getYesNoPercentage(result));
            }
            promise.complete();
        }).onFailure(promise::fail);

        return promise.future();
    }

    private Future<Void> addContextuality(JsonObject document, DocumentScope documentScope) {
        Promise<Void> promise = Promise.promise();

        JsonObject contextuality = new JsonObject();
        document.put("contextuality", contextuality);

        String catalogueId = document.getString("_id");

        Promise<Double> rightsAvailability = Promise.promise();
        dqvProvider.getRightsAvailability(catalogueId, documentScope, rightsAvailability);
        rightsAvailability.future().compose(result -> {
            if (result != -1.0) {
                contextuality.put("rightsAvailability", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getByteSizeAvailability(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                contextuality.put("byteSizeAvailability", getYesNoPercentage(result));
            }

            return Future.<Double>future(pr -> dqvProvider.getDateIssuedAvailability(catalogueId, documentScope, pr));
        }).compose(result -> {
            if (result != -1.0) {
                contextuality.put("dateIssuedAvailability", getYesNoPercentage(result));
            }
            return Future.<Double>future(pr -> dqvProvider.getDateModifiedAvailability(catalogueId, documentScope, pr));
        }).onSuccess(result -> {
            if (result != -1.0) {
                contextuality.put("dateModifiedAvailability", getYesNoPercentage(result));
            }
            promise.complete();
        }).onFailure(promise::fail);

        return promise.future();
    }

    private JsonArray getYesNoPercentage(Double yesPercentage) {
        double roundedYes = Math.round(yesPercentage);
        return new JsonArray()
                .add(new JsonObject()
                        .put("name", "yes")
                        .put("percentage", roundedYes))
                .add(new JsonObject()
                        .put("name", "no")
                        .put("percentage", 100.0 - roundedYes));
    }

    public void tearDown() {
        dbClient.close();
    }

}
