package io.piveau.metrics.cache.persistence;

public enum DocumentScope {
    GLOBAL, CATALOGUE, COUNTRY, SCORE
}
