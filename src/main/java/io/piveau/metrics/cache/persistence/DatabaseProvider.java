package io.piveau.metrics.cache.persistence;

import io.vertx.codegen.annotations.ProxyGen;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.List;

@ProxyGen
public interface DatabaseProvider {

    String SERVICE_ADDRESS = "metric.cache.database";

    static DatabaseProvider create(Vertx vertx, Handler<AsyncResult<DatabaseProvider>> readyHandler) {
        return new DatabaseProviderImpl(vertx, readyHandler);
    }

    static DatabaseProvider createProxy(Vertx vertx, String address) {
        return new DatabaseProviderVertxEBProxy(vertx, address);
    }

    void listDocuments(DocumentScope documentScope, Handler<AsyncResult<List<String>>> resultHandler);

    void getInfos(DocumentScope documentScope, Handler<AsyncResult<JsonArray>> resultHandler);

    void getDocument(DocumentScope documentScope, String id, Handler<AsyncResult<JsonObject>> resultHandler);

    void deleteDocument(DocumentScope documentScope, String id, Handler<AsyncResult<String>> resultHandler);

    void getScore(String resolution, String startDate, String endDate, String id, Handler<AsyncResult<JsonObject>> resultHandler);

    void refreshMetrics();

    void refreshSingleMetrics(String id);

    void clearMetrics();

    void getDistributionReachabilityDetails(String catalogueId, int offset, int limit, String lang, Handler<AsyncResult<JsonObject>> resultHandler);
}
